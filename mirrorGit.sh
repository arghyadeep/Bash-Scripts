#!/bin/sh 
# Script to mirror git repos
# Usage:
# ./mirrorGit.sh <original_repo_link> <new_repo_link> <original_repo_name>

git clone --mirror $1

cd $3.git

git push --mirror $2

# To Update later, use:
# git remote update
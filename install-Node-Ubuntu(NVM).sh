#!/bin/sh 
# Script to install Node.js using NVM
# To run the script as sudo, execute the script as './install-Node-Ubuntu(NVM).sh sudo'


$1 apt install curl -y 
curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.35.3/install.sh -o install_nvm.sh
bash install_nvm.sh



export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"   # This loads nvm bash_completion

nvm install node

$1 apt install npm -v

echo ""
echo "~~~~~~~~~~~~Installation Complete~~~~~~~~~~~~"
    
echo ""

echo -n "Node.js Version : "
node -v

echo -n "Yarn Version : "
npm -v

echo ""
#!/bin/sh

$1 apt-get remove docker docker-engine docker.io containerd runc -y

$1 apt update
$1 apt install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common -y

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | $1 apt-key add -

$1 add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
$1 apt update
$1 apt install docker-ce docker-ce-cli containerd.io -y
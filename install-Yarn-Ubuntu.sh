#!/bin/sh
# Script to install yarn & Node.js
# To run the script as sudo, execute the script as './install-Yarn-Ubuntu.sh sudo'

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | $1 apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | $1 tee /etc/apt/sources.list.d/yarn.list

$1 apt update
$1 apt install yarn -y

# If Node.js is already installed through nvm, use this
# $1 apt install --no-install-recommends yarn

echo ""
echo "~~~~~~~~~~~~Installation Complete~~~~~~~~~~~~"
    
echo ""

echo -n "Node.js Version : "
node -v

echo -n "Yarn Version : "
yarn --version

echo ""